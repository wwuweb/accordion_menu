(function($, Drupal, window, document, undefined) {

   'use strict';

   Drupal.behaviors.accordion_menu = {
    attach: function(context, settings) {
      if (!('accordion_menu' in settings)) {
        return;
      }

      for (var key in settings.accordion_menu) {
        var deltas = key.match(/delta\-(\d+)/);

        if (deltas === null) {
          continue;
        }

        var delta = deltas[1]; // First capture group.
        var $accordion_menu = $('.accordion-menu-' + delta, context);
        var options = settings.accordion_menu[key];
        var active = options.active;
        var header = options.header;
        var max_depth = Number(options.menuDepth);
        var start_depth = 1;

        if ('startDepth' in options) {
          max_depth = Number(options.startDepth) + Number(options.menuDepth);
          start_depth = Number(options.startDepth);
        }

        options.header = header + '.item-depth-' + start_depth;
        $accordion_menu.accordion(options);

        for (var depth = start_depth; depth < max_depth; depth++) {
          var $accordion_content = $('.accordion-content-depth-' + depth, $accordion_menu);

          options.header = header + '.item-depth-' + (depth + 1);

          if (active !== false && settings.accordion_menu_active_trail[key][depth]) {
            var menu_mlid = settings.accordion_menu_active_trail[key][depth];
            var $link = $('menu-mlid-' + menu_mlid, $accordion_content);

            options.active = $accordion_content.find(options.header).index($link);
            $link.addClass('active-trail');
          }
          else {
            options.active = false;
          }

          $accordion_content.accordion(options);
        }

        $('.accordion-header.no-children', $accordion_menu)
          .unbind('.accordion')
          .children('.ui-icon')
          .removeClass(options.icons.header)
          .addClass(options.icons.empty);
      }
    }
  };

}(jQuery, Drupal, this, this.document));
