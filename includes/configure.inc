<?php
/**
 * @file
 * Provides block configuration routines.
 *
 * @author Jim Berry ("solotandem", http://drupal.org/user/240748)
 */

/**
 * Implements hook_block_configure().
 */
function _accordion_menu_block_configure($delta) {
  // Gather information.
  $config = accordion_menu_config($delta);
  $options = array('' => '- None -') + menu_get_menus();
  if (module_exists('menu_block')) {
    // Add menu_block sub-menus.
    $menus = menu_block_block_info();
    $block_options = array('' => '- None -');
    foreach ($menus as $delta => $menu) {
      $mb_config = menu_block_get_config($menu['info']);
      $block_options[$delta] = $menu['info'];
    }
  }
  else {
    drupal_set_message(t('If you enable the !module module, then you may display menu blocks with an accordion effect.', array('!module' => l('Menu Block', 'https://drupal.org/project/menu_block'))), 'warning');
  }

  $max_accordion_depth = $config['max_depth'] - 1;

  if (isset($config['start_depth'])) {
    $max_accordion_depth = $config['max_depth'] - $config['start_depth'];
  }

  $options_depth = drupal_map_assoc(range(1, $max_accordion_depth));

  // Build form.
  $fs1['basic_options'] = array(
    '#type' => 'fieldset',
    '#title' => t('Basic options'),
    '#description' => t('Basic Accordion option.'),
    '#tree' => FALSE,
  );
  $form1['accordion_menu_menu_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Menu Name'),
    '#default_value' => $config['menu_name'],
  );
  $form1['accordion_menu_menu_source'] = array(
    '#type' => 'select',
    '#title' => t('Menu source'),
    '#description' => t('Select the menu to display as an accordion.'),
    '#default_value' => $config['menu_source'],
    '#options' => $options,
    '#ajax' => array(
      'callback' => '_accordion_menu_form_menu_depth_options',
      'wrapper' => 'accordion-menu-depth',
      'replace' => 'replace',
    ),
  );
  if (module_exists('menu_block')) {
    $form1['accordion_menu_block_source'] = array(
      '#type' => 'select',
      '#title' => t('Menu block source'),
      '#description' => t('Select the menu block to display as an accordion. This will override the menu source above.'),
      '#default_value' => $config['block_source'],
      '#options' => $block_options,
      '#ajax' => array(
        'callback' => '_accordion_menu_form_menu_depth_options',
        'wrapper' => 'accordion-menu-depth',
        'replace' => 'replace',
      ),
    );
  }
  $form1['accordion_menu_menu_depth'] = array(
    '#type' => 'select',
    '#title' => t('Accordion depth'),
    '#description' => t('Select the depth to which the accordion must propagate.'),
    '#default_value' => $config['menu_depth'],
    '#options' => $options_depth,
    '#prefix' => '<div id="accordion-menu-depth">',
    '#suffix' => '</div>',
  );
  $form1['accordion_menu_script_scope'] = array(
    '#type' => 'textfield',
    '#title' => t('Script scope'),
    '#description' => t('The location in which to place the script. The location must be implemented by the theme. Values include "<strong>header</strong>" and "<strong>footer</strong>" by default.'),
    '#default_value' => $config['script_scope'],
    '#size' => 10,
  );
  $form1['accordion_menu_header_link'] = array(
    '#type' => 'checkbox',
    '#title' => t('Header link'),
    '#description' => t('If checked, header menu items will be output as links. Clicking on the menu item may trigger the accordion effect and a redirect to the header link. <strong>Undesirable</strong>'),
    '#default_value' => $config['header_link'],
  );
  $form1['accordion_menu_menu_expanded'] = array(
    '#type' => 'checkbox',
    '#title' => t('Expand menu'),
    '#description' => t('If checked, menu items in the active trail will be expanded. Otherwise, menus will not be expanded on page load.'),
    '#default_value' => $config['menu_expanded'],
  );
  $fs1['basic_options'] += $form1;

  $fs2['jquery_advanced'] = array(
    '#type' => 'fieldset',
    '#title' => t('Advanced options'),
    '#description' => t('Advanced Accordion options.'),
    '#tree' => FALSE,
  );
  $form2['accordion_menu_header'] = array(
    '#type' => 'textfield',
    '#title' => t('Header'),
    '#description' => t('Selector for the header element.'),
    '#default_value' => $config['header'],
    '#size' => 15,
  );
  $form2['accordion_menu_animated'] = array(
    '#type' => 'textfield',
    '#title' => t('Animation'),
    '#description' => t('Enter the name of an animation, or set to "false" to disable them. In addition to the default of "<strong>slide</strong>", "bounceslide" and all defined easing methods are supported ("bounceslide" requires UI Effects Core). With menu items, the alternative effects are mostly unnoticeable.'),
    '#default_value' => $config['animated'],
    '#size' => 10,
  );
  $form2['accordion_menu_header_icon'] = array(
    '#type' => 'textfield',
    '#title' => t('Header icon'),
    '#description' => t('Enter the name of the icon to use on a header. jQuery recommends using the icons native to the jQuery UI CSS Framework. Default value is "<strong>ui-icon-triangle-1-e</strong>."'),
    '#default_value' => $config['header_icon'],
  );
  $form2['accordion_menu_selected_icon'] = array(
    '#type' => 'textfield',
    '#title' => t('Selected header icon'),
    '#description' => t('Enter the name of the icon to use on a selected header. jQuery recommends using the icons native to the jQuery UI CSS Framework. Default value is "<strong>ui-icon-triangle-1-s</strong>."'),
    '#default_value' => $config['selected_icon'],
  );
  $form2['accordion_menu_empty_icon'] = array(
    '#type' => 'textfield',
    '#title' => t('Empty header icon'),
    '#description' => t('Enter the name of the icon to use on an empty header (i.e., no children). See !link for the icons native to the jQuery UI CSS Framework."', array('!link' => l('this page', 'http://jqueryui.com/themeroller/'))),
    '#default_value' => $config['empty_icon'],
  );
  $form2['accordion_menu_event'] = array(
    '#type' => 'select',
    '#title' => t('Event'),
    '#description' => t('The event on which to trigger the accordion. Suggested use: If the top-level menu item does not link to content, then select "click" to expand the menu. If the top-level menu item links to content, then select "mousedown" to expand the menu and display the linked content. Use "mouseover" at your own risk.'),
    '#default_value' => $config['event'],
    '#options' => drupal_map_assoc(array('click', 'mousedown', 'mouseover')),
  );
  $form2['accordion_menu_collapsible'] = array(
    '#type' => 'checkbox',
    '#title' => t('Collapsible'),
    '#description' => t('Whether all the sections can be closed at once. Allows collapsing the active section by the triggering event.'),
    '#default_value' => $config['collapsible'],
  );
  $form2['accordion_menu_icons'] = array(
    '#type' => 'checkbox',
    '#title' => t('Icons'),
    '#description' => t('If checked, then the icons specified above will be used on header elements. Uncheck to omit the icons.'),
    '#default_value' => $config['icons'],
  );
  $form2['accordion_menu_height_style'] = array(
    '#type' => 'textfield',
    '#title' => t('Height Style'),
    '#description' => t('Controls the height of the accordion and each panel. Possible values are: <strong>"auto"</strong> to set all panels to the height of the tallest panel, <strong>"fill"</strong> to expand to the available height based on the accordion\'s parent height. and <strong>"content"</strong> to make each panel only as tall as its content'),
    '#default_value' => $config['height_style'],
  );
  $fs2['jquery_advanced'] += $form2;

  // Build the form.
  $form['tabs'] = array(
    '#type' => 'vertical_tabs',
    '#default_tab' => 'edit-directories',
  );
  $form['tabs']['basic'] = $fs1['basic_options'];
  $form['tabs']['advanced'] = $fs2['jquery_advanced'];

//   $form['#validate'][] = 'accordion_menu_block_validate';

  return $form;
}

/**
 * Form validation handler for hook_block_configure().
 */
function _accordion_menu_block_validate($form, &$form_state) {
  foreach (array('animated', 'script_scope') as $key) {
    $value = $form_state['values']['accordion_menu_' . $key];

    if (preg_match('@[^a-z]@', strtolower($value))) {
      form_set_error('accordion_menu_' . $key, t('Enter a string with only letters. (' . $key . ')'));
    }
  }

  $header = $form_state['values']['accordion_menu_header'];
  preg_match('@^([a-z]+([1-9]|)).*@', strtolower($header), $matches);

  if (empty($matches) || $matches[0] != $matches[1]) {
    form_set_error('accordion_menu_header', t('Enter a valid HTML tag. (header)'));
  }

  $menu_source = $form_state['values']['accordion_menu_menu_source'];
  $block_source = isset($form_state['values']['accordion_menu_block_source']) ? $form_state['values']['accordion_menu_block_source'] : NULL;
  $menu_depth = $form_state['values']['accordion_menu_menu_depth'];

  if ($block_source) {
    if (empty($menu_source) && empty($block_source)) {
      form_set_error('accordion_menu_menu_source', t('Select either a menu source or a menu block source. (menu_source)'));
      form_set_error('accordion_menu_block_source', t('Select either a menu source or a menu block source. (block_source)'));
    }
  }
  elseif (empty($menu_source)) {
    form_set_error('accordion_menu_menu_source', t('Select a menu source. (menu_source)'));
  }

  if (empty($menu_depth) || $menu_depth === 'none') {
    form_set_error('accordion_menu_menu_depth', t('Select a depth of the menu for accordion.'));
  }
}

/**
 * Implements hook_block_save().
 */
function _accordion_menu_block_save($delta, $edit) {
  if (!empty($delta)) {
    $config = accordion_menu_config();
    unset($config['delta']);
    // Save the block configuration settings.
    foreach ($config as $key => $value) {
      variable_set("accordion_menu_{$delta}_{$key}", $edit['accordion_menu_' . $key]);
    }
  }
}
